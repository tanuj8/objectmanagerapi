from bson.json_util import dumps
from bson.objectid import ObjectId
from flask import jsonify, request
from datetime import datetime
from briqy.errors import BriqError, handle_briq_error, handler
from briqy.flaskHelper import authorize, response
from werkzeug.utils import send_from_directory
from DB_config import collection
from flask_swagger_ui import get_swaggerui_blueprint


def send_static(path):
  return send_from_directory('static',path)

### swagger specific ###
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "objectmanagerapi"
    }
)


@handler._try
def home():
  #authorize(request)
  return jsonify("Ok")

@handler._try
def add_Object():
    
    _json = request.json

    #required_parameters = ['object_name']
    #authorize(request, required_parameters)

    if request.method == 'POST':
        
        context = {
            'object_name':  _json['object_name'],
            'company_id' : _json['company_id'],
            'date_created' : '{:%d.%m.%Y %H:%M:%S}'.format(datetime.now()),
            'date_updated' : '{:%d.%m.%Y %H:%M:%S}'.format(datetime.now()),
            'last_modified_by' : _json["last_modified_by"],
            'description' : _json["description"],
            'object_type' : _json["object_type"],

            'columns' : _json['columns'],

            'relationships' : _json['relationships']
        }
        
        collection.insert_one(context)
  
        return response({
          "message" : "Object added successfully",
        }, 201)

    else:
        return response({
          "message" : "Enter Correct Information",
        },400)

@handler._try
def showAll():
    #authorize(request)
    return dumps(collection.find())  

@handler._try
def show_Object_by_ID(_id):
    obj = {'_id': ObjectId(_id)}
    return dumps(collection.find(obj))

@handler._try
def deleteObject(_id):
    obj = {'_id': ObjectId(_id)}
    collection.delete_one(obj)
    
    return response({
          "message" : "Object deleted successfully",
        }, 200)

@handler._try
def updateObject(_id):
    
    _json = request.json
   
    x = dict(
    object_name = _json["object_name"],
    company_id = _json["company_id"],
    last_modified_by = _json["last_modified_by"],
    description = _json["description"],
    object_type = _json["object_type"],
    )
 
    #collection.update_one({'_id': ObjectId(_id) },{"$set": { "object_name" :  _json["object_name"]} })  
    collection.update_one({'_id': ObjectId(_id) },{"$set": x })  
    collection.update_one({'_id': ObjectId(_id) },{"$set":{'date_updated' : '{:%d.%m.%Y %H:%M:%S}'.format(datetime.now()),}})
    
    return response({
          "message" : f"Object { ObjectId(_id)} Updated successfully"
        }, 200 )


