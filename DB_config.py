from flask_pymongo import PyMongo
from pymongo import MongoClient

USER_ID = 'tanuj123'
PASSWORD = 'tanuj123'
DATABASE = 'ObjectManager'

DB_URI = "mongodb+srv://{}:{}@cluster0.gqw3j.mongodb.net/{}?retryWrites=true&w=majority".format(USER_ID,PASSWORD,DATABASE)

cluster =  MongoClient(DB_URI)
db = cluster["ObjectManager"]
collection = db["objectmanager"]

# Other way to connect
# api.config['MONGO_URI'] = "mongodb+srv://{}:{}@cluster0.gqw3j.mongodb.net/{}".format(USER_ID,PASSWORD,DATABASE)
# mongo = PyMongo(api)
# collection = mongo.db.objectmanager
