from briqy.errors import handler, handle_briq_error, BriqError
from briqy.flaskHelper import authorize, response, health_endpoints
from briqy.errors import BriqError, handle_briq_error, handler
from briqy.flaskHelper import authorize, health_endpoints, response
from briqy.logHelper import b
from briqy.varz import get_gcs_client

from flask import Flask
from flask_pymongo import PyMongo
from bson.json_util import dumps
from flask import Flask, make_response
from flask import jsonify, request
import views

api = Flask(__name__)

api.register_blueprint(health_endpoints)
api.register_error_handler(BriqError, handle_briq_error)
api.register_error_handler(404, handle_briq_error)
api.register_error_handler(405, handle_briq_error)
api.register_error_handler(400, handle_briq_error)

api.add_url_rule('/', view_func=views.home, methods=["GET"])
api.add_url_rule('/object/add', view_func=views.add_Object, methods=["POST"])
api.add_url_rule('/object/getAll', view_func=views.showAll, methods=["GET"])
api.add_url_rule('/object/<_id>', view_func=views.show_Object_by_ID, methods=["GET"])
api.add_url_rule('/object/<_id>', view_func=views.deleteObject, methods=["DELETE"])
api.add_url_rule('/object/<_id>', view_func=views.updateObject, methods=["PUT"])

### Swagger API ###
api.add_url_rule('/static/<path:path>',view_func=views.send_static)
api.register_blueprint(views.SWAGGERUI_BLUEPRINT, url_prefix=views.SWAGGER_URL)


if __name__ == "__main__":
    #api.run()
    api.run(host='127.0.0.1', port=8080, debug=True)