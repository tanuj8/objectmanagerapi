import pymongo
from pymongo import MongoClient
from pymongo import collection
from flask import Flask, make_response
from flask_pymongo import PyMongo
from bson.json_util import JSONOptions, dumps
from bson.objectid import ObjectId  # Generate random string which will be the ID for records
from flask import jsonify, request
from werkzeug.security import generate_password_hash, check_password_hash
from DB_config import USER_ID,PASSWORD,DATABASE,DB_URI
from datetime import datetime

app = Flask(__name__)

# DB Configurations

cluster =  MongoClient(DB_URI)
db = cluster["ObjectManager"]
collection = db["objectmanager"]

#@app.route('/add',methods=['POST'])
def add_Object():
    
    _json = request.json
    _object_name = _json["object_name"]
    _id = _json["_id"]
    _company_id = _json["company_id"]
    _last_modified_by = _json["last_modified_by"]
    _description = _json["description"]
    _object_type = _json["object_type"]

    if request.method == 'POST':
        
        obj = {
            'object_name': _object_name,
            '_id' : _id,
            'company_id' : _company_id,
            'date_created' : datetime.now(),
            'date_updated' : datetime.now(),
            'last_modified_by' : _last_modified_by,
            'description' : _description,
            'object_type' : _object_type,

            "columns" : [
            {
              "name" : "Company_Code",
              "name_friendly" : "Company_Code",
              "data_type" : "STRING",
              "description" : "This is a description for the company_code field in the Job object",
              "periodic" : True,
              "formula" : "LEFT(Cost_Code, 3)"
            },
            {
              "name" : "Cost_Code",
              "name_friendly" : "Cost_Code",
              "data_type" : "STRING",
              "description" : "This is a description for the Cost_Code field in the Job object",
              "periodic" : False,
              "formula" : "LEFT(Cost_Code, 3)"
            }
          ],
          "relationships" : [
            {
              "name" : "relationship1",
              "description" : "here is a description",
              "object" : {
                "name" : "SomeOtherObject",
                "id" : "OtherObjectsId"
              }
            }
          ]
        }

        collection.insert_one(obj)
        return make_response(jsonify("Object added successfully"), 201)

    else:
        return jsonify("Enter Correct Info")

app.add_url_rule('/add', view_func=add_Object, methods=["POST"])

#@app.route('/showAll',methods=['GET'])
def showAll():
    return jsonify(str(list(collection.find())))

app.add_url_rule('/showAll', view_func=showAll, methods=["GET"])

#@app.route('/show',methods=['POST'])
def show_Object_by_ID():
    _json = request.json
    _id = _json['_id']
    obj = {'_id': _id}
    return jsonify(str(list(collection.find(obj))))

app.add_url_rule('/show', view_func=show_Object_by_ID, methods=["POST"])

#@app.route('/delete/<_id>',methods=['DELETE'])
def deleteObject(_id):
    obj = {'_id': _id}
    collection.delete_one(obj)
    return make_response(jsonify("Object deleted successfully"), 200)

app.add_url_rule('/delete/<_id>', view_func=deleteObject, methods=["DELETE"])

#@app.route('/update/<_id>',methods=['PUT'])
def updateObject(_id):
    _json = request.json
    _object_name = _json['object_name']

    collection.update_one({'_id':_id },{"$set":{'object_name':_object_name ,'date_updated' : datetime.now()}})
    
    return jsonify(str(list(collection.find({'_id': _id}))))

app.add_url_rule('/update/<_id>', view_func=updateObject, methods=["PUT"])

if __name__ == "__main__":
    app.run(debug=True)