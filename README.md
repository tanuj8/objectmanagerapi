# Object Manager

### Supported Routes and It's corresponding methods :
---
|DESCRIPTION         | ROUTES      | METHODS |
|---                 | ---         |---      |
|Add New Object      |/add         |POST     | 
|Show all Objects    |/showAll     |GET      |
|Show a single object|/show/<_id>  |GET      |
|Delete object       |/delete/<_id>|DELETE   |
|Update object       |/update/<_id>|PUT      |

## Local Configuration 
---

**One Way :**
```
source config.sh file
```

**Other Way (Manual Setup) :**

```
1. Create a virtual environment : virtualenv env
2. locate to env\scripts\activate 
3. Install the dependencies : pip install -r requirements.txt
4. Move of the env folder and run : python api.py
```

